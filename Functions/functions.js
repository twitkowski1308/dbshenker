var navigationBar = require('../PageObjects/navigationBar.js');
var joinUs = require('../PageObjects/joinUs.js');
var functions = function() {
  this.loadPage = function() {
    browser.waitForAngularEnabled(false);
    browser.driver.manage().window().maximize();
    browser.get('https://www.dbschenker.com/tscwarsaw/pl-pl');
  }

  this.justAFewStepsClick = function() {
    browser.actions()
      .mouseMove(navigationBar.buttons.joinUs).mouseMove(navigationBar.buttons.joinUs).mouseMove(navigationBar.buttons.justAFewSteps).click().perform();
  }

  const checkIfPresent = function(selector) {
    browser.wait(function () {
      return selector.isDisplayed();
    })
  }

  this.clickNextSlide = function() {
    browser.actions()
      .mouseMove(joinUs.carousel.prev).mouseMove(joinUs.carousel.next).click().perform();
  }

  this.checkIfSlideContainsTitle = function(slideNumber, title) {
    checkIfPresent(slideNumber)
    expect(slideNumber.getText()).toContain(title);
  }

  this.closeBrowser = function() {
    browser.close();
  }
};
module.exports = new functions();
