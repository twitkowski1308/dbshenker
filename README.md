# README #

Tests of carousel

### What is this repository for? ###

* repo for tests of carousel for DBShenker

### How do I get set up? ###

* open cmd or terminal
* clone the repo with "git clone..." url in the repo
* run "npm install webdriver-manager"
* run "webdriver-manager update"
* run "webdriver-manager start" 
* open another terminal or cmd window or tab
* run "npm install -g protractor"
* run "protractor conf.js"
