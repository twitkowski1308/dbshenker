var navigationBar = function() {
  this.buttons = {
    joinUs: element(by.xpath("(//a[@href='/tscwarsaw/pl-en/join-us'])[1]")),
    justAFewSteps: element(by.xpath("//span[text()='Just a few steps...']"))
  }
};
module.exports = new navigationBar();
