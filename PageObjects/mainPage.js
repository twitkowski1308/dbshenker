var mainPage = function() {
  this.elements = {
    cookieOk: element(by.xpath('/html/body/footer/div/div/div[4]/div[1]/div[3]/div[2]/button')),
    langEn: element(by.xpath("(//div[@class='country-selector__languages-link']//a)[2]"))
  }
};
module.exports = new mainPage();
