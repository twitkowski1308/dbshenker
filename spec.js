// spec.js
var functions = require('./Functions/functions.js');
var joinUs = require('./PageObjects/joinUs.js');
var mainPage = require('./PageObjects/mainPage.js');

describe('DBShenker', function() {
  it('Should check if carousell is working correctly', function() {
    functions.loadPage()
    mainPage.elements.langEn.click();
    functions.justAFewStepsClick()
    mainPage.elements.cookieOk.click()
    functions.checkIfSlideContainsTitle(joinUs.carousel.slide1,"1: Send Application")
    functions.clickNextSlide()
    functions.checkIfSlideContainsTitle(joinUs.carousel.slide2,"2: Phone Call")
    functions.clickNextSlide()
    functions.checkIfSlideContainsTitle(joinUs.carousel.slide3,"3: Personal Meeting")
    functions.clickNextSlide()
    functions.checkIfSlideContainsTitle(joinUs.carousel.slide4,"4: Video Conference")
    functions.clickNextSlide()
    functions.checkIfSlideContainsTitle(joinUs.carousel.slide5,"5: Job Offer")
    functions.closeBrowser()
  });
});
